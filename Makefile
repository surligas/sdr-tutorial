 .PHONY : pdf
pdf:
	cd 0.SDR-Intro && make $@
	cd 1.DSP-Basics && make $@
	cd 2.Modulation && make $@
	cd 3.GNU-Radio-Programming && make $@

.PHONY : clean
clean:
	cd 0.SDR-Intro && make $@
	cd 1.DSP-Basics && make $@
	cd 2.Modulation && make $@
	cd 3.GNU-Radio-Programming && make $@
