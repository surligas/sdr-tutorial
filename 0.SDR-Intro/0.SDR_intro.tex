\documentclass{beamer}
\usetheme{metropolis}
\metroset{progressbar=frametitle, background=light}
\usepackage{appendixnumberbeamer}
\usepackage{textcomp}
\usepackage{ marvosym }
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\usepackage[absolute,overlay]{textpos}
\usepackage{graphicx}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\title{Introduction to Software Defined Radios}
%\date{\today}
\author{Manolis Surligas \quad \newline\href{mailto:manolis@libre.space}{manolis@libre.space}}
\institute{Libre Space Foundation \& Computer Science Department, University of Crete}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\begin{document}
{
\setbeamercolor{background canvas}{bg=black}
\maketitle
}

\begin{frame}[standout]
\begin{center}
This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License

\ccbyncsaeu

\vspace{1em}
\footnotesize
\url{https://gitlab.com/surligas/sdr-tutorial}
\url{https://gitlab.com/surligas/gr-tutorial}
\end{center}
\end{frame}

\metroset{progressbar=frametitle, background=light}

\begin{frame}{What is a Software Defined Radio?}
According to ITU-R SM.2152, Software-defined  Radio  (SDR) is:
\begin{exampleblock}{}
A  radio  transmitter  and/or  receiver  employing  a  technology  that  allows the RF operating parameters including, but not limited to, frequency range, modulation type, or output power to be set or altered by software, excluding changes to operating parameters which occur during the normal pre-installed and predetermined operation of a radio according to a system specification or standard.”
\end{exampleblock}

\end{frame}


\begin{frame}{Software Defined Radios}
\textbf{The key characteristics are:}
\begin{itemize} \itemsep 1em
\item Multiple communication standards support
\item Easy and economic upgrades
\item More sophisticated RF devices \MVRightarrow ~~\textbf{Cognitive Radios}
\item Easy and rapid development, testing and deployment of new telecommunication
standards
\item Flexibility
\end{itemize}
\end{frame}

\begin{frame}{Software Defined Radios}
\begin{figure}[h]
\centering
\includegraphics[width=0.8\linewidth]{images/prog_vs_special.pdf} 
\end{figure}

\begin{block}{}
Depending on the application requirements software may run in various execution environments
\end{block}
\end{frame}



\begin{frame}{SDR: The hardware side}
\begin{figure}
\includegraphics[width=0.8\textwidth]{images/sdr_hardware.pdf}
\caption{A typical SDR hardware architecture}
\end{figure}
\end{frame}

\begin{frame}{SDR: The hardware side}
\begin{figure}
\includegraphics[width=0.95\textwidth]{images/superheterodyne.png}
\caption{A Superheterodyne transceiver}
\end{figure}
\end{frame}

\begin{frame}{SDR: The hardware side}
\begin{figure}
\includegraphics[width=0.8\textwidth]{images/zif.png}
\caption{A Zero-IF front-end}
\end{figure}
\end{frame}

\begin{frame}{SDR: The hardware side}
\begin{figure}
\includegraphics[width=0.8\textwidth]{images/direct_sampling.pdf}
\caption{Direct sampling receiver}
\end{figure}
\end{frame}

\begin{frame}{SDR: The software side}

\textbf{Complete SDR Platforms:}
\begin{itemize}
\item GNU Radio
\item Matlab Simulink
\item Pothos SDR
\end{itemize}

\textbf{Libraries:}
\begin{itemize}
\item Liquid-dsp
\item VOLK
\item itpp
\end{itemize}
\end{frame}

\begin{frame}{SDR: The software side}
\begin{itemize} \itemsep 1em
\item Software language can be arbitrary
\item For realtime applications C/C++
\begin{itemize}
\item Often assisted by Single Instruction Multiple Data (SIMD)
\end{itemize}
\item If the requirements are strict enough, FPGAs are used
\begin{itemize}
\item High throughput
\item Low latency, low jitter
\item Predictable timings
\end{itemize}
\end{itemize}
\end{frame}

\section{Introduction to GNU Radio}
\begin{frame}{What is GNU Radio?}

\begin{itemize} \itemsep 1.5em
\item GNU Radio is an open-source platform that provides signal processing blocks to implement software radios
\item Core written in C/C++, some Python bindings
\item Provides a GUI called \textbf{GNU Radio Companion (GRC)} to easily create software radio programs
\item \url{www.gnuradio.org}
\end{itemize}

\end{frame}


\begin{frame}
\begin{itemize} \itemsep 1.5em
\item It is highly recommended to install GNU Radio from the provided packages of your distribution
\begin{itemize}
\item \textbf{Ubuntu-Debian:} \textit{apt-get install gnuradio gnuradio-dev }
\item \textbf{Fedora:} \textit{yum install gnuradio gnuradio-devel}
\item \textbf{OpenSUSE:} \textit{zypper in gnuradio gnuradio-devel}
\item Pre-build Win64 images are also available 
\end{itemize}

\begin{alertblock}{}
More info at \url{https://wiki.gnuradio.org/index.php/InstallingGR}
\end{alertblock}


\end{itemize}

\end{frame}

%\begin{frame}{The lab session}
%
%\begin{itemize} \itemsep 1em
%\item Ten PCs with Linux and everything you may need
%\item You \textbf{prepare} the assignments \textbf{at home} 
%\item At the lab you just run them, ask questions, prove that you know what you are doing, get a grade
%\item \textbf{Attendance:}
%\begin{itemize}
%\item Lab sessions are \textbf{obligatory}
%\item It is ok to loose one, but not two or more
%\end{itemize}
%\item \textbf{Cheating:}
%\begin{itemize}
%\item Plagiarism in the submitted code leads to a grade of \textbf{zero}
%\item If you help a classmate during the lab session, the grade of \textbf{both} cheaters is halved 
%\end{itemize}
%\end{itemize}
%\end{frame}


\begin{frame}{The first GNU Radio application}

\begin{itemize} \itemsep 1.5em
\item Lets write our first software radio application with GNU Radio
\item Firstly, open \textbf{GNU Radio Companion} or \textbf{GRC}
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{images/grc_1.png}
\end{figure}

\end{frame}

\begin{frame}{The first GNU Radio application}

\begin{itemize} \itemsep 1.5em
\item This is the working area of GNU Radio
\item A program based on GNU Radio is a scenario with multiple processing units connected each other. It is commonly called \textbf{Flowgraph}
\item Each processing unit is called \textbf{Block}
\item Ready to use blocks can be found at the left side of GRC window
\begin{itemize}
\item \textbf{Ctrl+F} function is supported!
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1em
\item The option block contains several parameters related with the flowagraph
\item To reveal the properties of each block, double click on it
\item The important to remember:
\begin{itemize}
\item \textbf{ID:} The name of the Python executable that is going to be generated
\item \textbf{Generate Options:} QT GUI in case our flowgraph has a GUI element, NO GUI otherwise
\end{itemize}
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{images/grc_2.png}
\end{figure}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.5em
\item Now lets do some real work!
\item Suppose we want to add two \textbf{float} signals into one and plot them at the time domain each one and their sum
\item The first signal A will be a cosine with frequency of 2 kHz and the second signal B will be a sine of frequency 5 kHz
\item Their maximum amplitude should be 1
\item Search for a block called \textbf{Signal Source}
\item Drag and drop it at the working area
\end{itemize}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize}
\item The result:
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{images/grc_3.png}
\end{figure}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item Drag and drop or copy and paste (yeah Ctrl+C - Ctrl+V works on blocks!) and the second \textbf{Signal Source}
\item Lets set properly their parameters by double click on each one
\end{itemize}

\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_4.png} 
    \end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_5.png} 
    \end{figure}
\end{column}
\end{columns}

\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item Is our flowgraph ready? \textbf{NO!}
\item Each flowgraph should have at least one \textbf{source} block and at least one \textbf{sink}
\item Sources are blocks with only outputs. They only produce items
\item On the other hand, sinks have only inputs. They only consume items
\item We want to plot the time domain of the signals, so import a \textbf{QT Time Sink block}
\end{itemize}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item Make the appropriate configuration at the time sink block
\item Float inputs, 3 different inputs, proper labels e.t.c
\end{itemize}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_6.png} 
    \end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_7.png} 
    \end{figure}
\end{column}
\end{columns}

\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item Now we want to connect the output of each signal to the corresponding input of the time sink
\item Piece of cake! Just click on the desired source and then at the target sink port!
\item A connection is created. Move wherever you want the blocks. The connection follows!
\item But wait! We want also the sum of Signal A and Signal B. No problem! Bring in an \textbf{Add block}.
\end{itemize}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item After connecting the addition block you may end in a situation depicted in the figure below
\item Connections marked with read arrows are wrong and the flowgraph can not be generated into an executable
\end{itemize}

\begin{figure}[t]
        \centering
        \includegraphics[width=0.6\textwidth]{images/grc_8.png} 
\end{figure}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item In GNU Radio two connected ports \textbf{MUST} have the same size and type
\item Each port's data type is marked with a different color
\item To see the color mapping go to \textbf{Help $\Rightarrow$ Types }
\end{itemize}

\begin{columns}
\begin{column}{0.7\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_8.png} 
    \end{figure}
\end{column}

\begin{column}{0.3\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=0.8\textwidth]{images/grc_9.png} 
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{The first GNU Radio application}
\begin{itemize} \itemsep 1.2em
\item Just alter the data type of the addition block by changing its properties
\item Input/output data types can by altered also by selecting the desired block
and pressing the \textuparrow \textdownarrow keys
\end{itemize}

\begin{figure}[t]
        \centering
        \includegraphics[width=0.6\textwidth]{images/grc_10.png} 
\end{figure}
\end{frame}

\begin{frame}{The first GNU Radio application: Throttling }
\begin{itemize} \itemsep 1.2em
\item No we are ready to generate the executable of the flowgraph
\item To do this, click the \textbf{Generate} button
\item You may need to save the flowgraph file first
\item Unfortunately, during the generation of the executable a warning message appears
\end{itemize}
\begin{alertblock}{}
\textit{Warning: This flow graph may not have flow control: no audio or RF hardware blocks found. Add a Misc$\Rightarrow$Throttle block to your flow graph to avoid CPU congestion.}
\end{alertblock}
\end{frame}

\begin{frame}{The first GNU Radio application: Throttling}
\begin{itemize} \itemsep 1em
\item Lets explain this warning
\item The flowgraph does not include any hardware device with a specific rate of producing or consuming samples
\item There is no way to slow down the flowgraph. It will execute in maximum speed taking all the CPU resources
\item With all the CPU resources saturated, the host computer becomes unusable
\item The solution is the use of a \textbf{Throttle Block}
\end{itemize}
\begin{alertblock}{Note!}
When performing simulations, each flowgraph should have at least one throttle block.
\end{alertblock}
\end{frame}

\begin{frame}{The first GNU Radio application: Throttling}
\begin{itemize} \itemsep 0.5em
\item Throttle block will slow down each sample at the specified sampling period
\item How it works:
\begin{itemize}
\item Assume a sampling rate of 32 KSPS (Kilo-Samples per Second)
\item This means that the system should be able process 32000 samples each second
\item If the CPU freely executed the flowgraph may produce more samples per second
\item Throttle block, slows down the processing of samples by sleeping an amount of time after each sample
\item In our case the sample duration is $\frac{1}{32000} = 31.25$ microseconds
\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}{The first GNU Radio application: Throttling}
\begin{figure}[t]
  \centering
  \includegraphics[width=0.5\textwidth]{images/throttle.png} 
\end{figure}
\begin{itemize} \itemsep1.5em
\item Add the throttle block and generate the flowgraph again
\item Execute the flowgraph either pressing the \textbf{Execute} button, or running the generated python file form command line
\item Show time!
\end{itemize}
\end{frame}


\begin{frame}{The first GNU Radio application}
\begin{block}{Question 1}
Almost every block takes as argument the sampling rate. Why? How the sampling rate is chosen?
\end{block}

\begin{block}{Question 2}
If the sampling rate is increased, how the throttle block will react? How about the CPU?
\end{block}
\end{frame}

\begin{frame}{The first GNU Radio application: Interacting with user input}
\begin{itemize} \itemsep1.5em
\item Ok, that was a nice first example but a little boring
\item Lets take as parameter the frequency of each signal
\item To achieve that insert two QT GUI Range widgets
\item Each one will specify the frequency of the corresponding signal source
\end{itemize}
\end{frame}

\begin{frame}{The first GNU Radio application: Interacting with user input}
\begin{itemize} \itemsep 1.5em
\item ID is used as variable name
\item At the desired block, place the ID of the corresponding GUI widget at the parameter field
\item As user changes from the graphical slider the frequency, the new value is automatically passed to the corresponding block
\end{itemize}
\end{frame}

\begin{frame}{The first GNU Radio application: Interacting with user input}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_11.png} 
    \end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}[t]
        \centering
        \includegraphics[width=1\textwidth]{images/grc_12.png} 
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{The first GNU Radio application: Interacting with user input}
\begin{block}{Question}
Which should be the stop frequency at the slider properties?
\end{block}
\end{frame}

%\begin{frame}{The course code repository}
%
%\begin{itemize} \itemsep 1.5em
%\item We will use a Git repository hosted on Github to provide code and existing flowgraphs
%\item It will updated frequently, so stay tuned!
%\item We will try for each lecture to provide corresponding GNU Radio flowgraphs
%\item \url{https://github.com/surligas/gr-cs330}
%\end{itemize}
%\end{frame}

%\begin{frame}{The course code repository}
%
%\begin{itemize} \itemsep 1.5em
%\item We will use a Git repository hosted on Github to provide code and existing flowgraphs
%\item Folder \textit{examples} contains complete, working examples
%\item Folder \textit{apps} will be used for the assignments, may contain incomplete \textbf{no} working flowgraphs
%\item \textbf{Feel free to push your own interesting/boring/whatever flowgraphs}
%\end{itemize}
%\end{frame}

%\begin{frame}{Lets hear some music!}
%\begin{itemize}
%\item \textbf{Use the fm\_receiver flowgraph in the \textit{examples} folder}
%\end{itemize}
%\end{frame}

\end{document}
