\documentclass{beamer}

\usetheme{metropolis}
\metroset{progressbar=frametitle, background=light}
\usepackage{appendixnumberbeamer}
\usepackage{textcomp}
\usepackage{ marvosym }
\usepackage[outputdir=build]{minted}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\usepackage[absolute,overlay]{textpos}
\usepackage{graphicx}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\usepackage[scale=2]{ccicons}
\usepackage{ marvosym }
\usepackage{graphicx}
\usepackage{listings}

\title{GNU Radio Programming}

%\date{\today}
\author{Manolis Surligas \quad \newline\href{mailto:surligas@csd.uoc.gr}{surligas@csd.uoc.gr}}
\institute{Computer Science Department, University of Crete}


\begin{document}

{
\setbeamercolor{background canvas}{bg=black}
\maketitle
}

\begin{frame}[standout]
\begin{center}
This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License

\ccbyncsaeu

\vspace{1em}
\footnotesize
\url{https://gitlab.com/surligas/sdr-tutorial}
\url{https://gitlab.com/surligas/gr-tutorial}
\end{center}
\end{frame}

\begin{frame}{Extending GNU Radio}
\begin{itemize} \itemsep 1.5em
\item GNU Radio can be extended with additional functionality with two ways:
\begin{enumerate} \itemsep 1em
\item In-tree development
\item Out-of-tree (OOT) modules
\end{enumerate}
\item In the class we will develop a custom OOT module
\end{itemize}
\end{frame}


\begin{frame}{GNU Radio OOT-modules}
\begin{itemize} \itemsep 1.5em
\item OOT modules are GNU Radio components that do not belong to the GNU Radio source tree
\end{itemize}
\textbf{Advantages:}
\begin{itemize} \itemsep 1em
\item Easily maintained by individual developers
\item Easy installation of multiple OOT modules
\item Small and fast compilation units
\item Seamless integration in the GRC
\end{itemize}

\begin{block}{}
CGRAN (\url{http://cgran.org}) is a place with public available GNU Radio OOT modules. 
\end{block}
\end{frame}

\begin{frame}[fragile]{Writing the first OOT module}
\begin{itemize} \itemsep 1.5em
\item In order to create an OOT module some files and folders should be created
\item The process is automated and the necessary files are produced by the \textbf{gr\_modtool} tool
\item To create a new OOT module with the name \textbf{test} execute:
\begin{lstlisting}[language=bash]
$ gr_modtool newmod test
\end{lstlisting}
\item This will create the module folder with name \textbf{gr-test}
\end{itemize}
\end{frame}

\begin{frame}{OOT module structure}
\begin{itemize} \itemsep 1.5em
\item Each OOT module consists from a set of folders
\item Necessary are the folders:
\begin{itemize}
\item[-] \textbf{include:} contains the public interfaces of the module classes and blocks
\item[-] \textbf{lib:} contains the implementation files of the module classes and blocks. Can also contain private module classes
\item[-] \textbf{grc:} includes \textbf{.yml} files that are used from GRC to provide a graphical representation of a block
\item[-] \textbf{swig:} contains necessary files for the construction of the C++ to Python interface
\item[-] \textbf{python:} contains blocks written in Python and/or files for the proper organization of the C++ to Python interface
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{GNU Radio block types}
Depending the ratio between the items that a block consumes and produces, all possible blocks fall under 4 categories: 
\begin{itemize} \itemsep 1em
\item Synchronous blocks (1:1)
\item Interpolation blocks (1:N)
\item Decimation blocks (N:1)
\item Basic Blocks (M:N)
\end{itemize}
\end{frame}

\begin{frame}{Synchronous Blocks (1:1)}
\begin{itemize} \itemsep 1em
\item Blocks that consume and produce equal amount of items per port
\item Easy to write, easy to understand
\item In most cases, synchronous blocks are used
\item If a synchronous block has zero inputs, is called \textbf{Source}
\item If a synchronous block has zero outputs, is called \textbf{Sink}
\item Developers should override the \textbf{work()} method
\end{itemize}
\end{frame}

\begin{frame}{Interpolation Blocks (1:N)}
\begin{itemize} \itemsep 1em
\item Similar with the synchronous blocks
\item Fixed input-output ratio
\item For every input item, N output items in each port are produced
\item Developers should specify input-output ratio at the block constructor
\item Developers should override the \textbf{work()} method
\end{itemize}
\end{frame}

\begin{frame}{Decimation Blocks (N:1)}
\begin{itemize} \itemsep 1em
\item Fixed input-output ratio
\item For every N input item, 1 output item in each port is produced
\item Developers should specify input-output ratio at the block constructor
\item Developers should override the \textbf{work()} method
\end{itemize}
\end{frame}

\begin{frame}{Basic Blocks (M:N)}
\begin{itemize} \itemsep 1em
\item Arbitrary input-output ratio at any time instance of the program
\item Developers should specify both the number of items consumed and produced manually
\item Developers should override the \textbf{general\_work()} method
\item Great flexibility, hard to understand and develop
\item All other block types are derived from the basic block
\end{itemize}
\end{frame}

\begin{frame}{IO Signatures}
Each GNU Radio block at the constructor should provide:
\begin{itemize} \itemsep 1em
\item The number of input ports
\item The number of output ports
\item The size of the item at the corresponding port
\end{itemize}

\begin{block}{}
The above are also known as the IO signature of the block
\end{block}
\end{frame}

\begin{frame}[fragile]{IO Signatures}
\textbf{Example 0:} Declare exactly 2 ports, with complex numbers as items

\begin{minted}[frame=single, fontsize=\tiny]{cpp}
gr::io_signature::make(2, 2, sizeof(gr_complex));
\end{minted}

\textbf{Example 1:} Declare exactly 2 ports, the first with float items and the second with items consisting form 64 complex numbers

\begin{minted}[frame=single, fontsize=\tiny]{cpp}
gr::io_signature::make2(2, 2, sizeof(float),
                        64 * sizeof(gr_complex));
\end{minted}
\end{frame}

\begin{frame}{Items Processing}
Until now, we saw how to create blocks. But:
\begin{itemize} \itemsep 1em
\item How items from input ports are processed?
\item Who is responsible to feed the block with items?
\item How the processed items are propagated at the output ports?
\end{itemize}

\vspace{1em}
\begin{block}{}
GNU Radio scheduler is responsible to activate each block, depending if the requirements of the input and output ports are satisfied. This means if the previous blocks have produced enough items and their is space to write the output items. If this holds, GNU Radio scheduler automatically executes the \textbf{work()} or \textbf{general\_work()} method
\end{block}

\end{frame}
                
\begin{frame}[fragile]{The work() method}
\begin{minted}[frame=single, fontsize=\tiny]{cpp}
int
work(int noutput_items,
     gr_vector_const_void_star &input_items,
     gr_vector_void_star &output_items);
\end{minted}

\begin{itemize}
\item \textbf{noutput\_items:} The number of output items that this invocation can produce. Due to the fixed rate of input-output of synchronous, interpolation, decimation blocks the number of available input items can be easily retrieved
\item \textbf{input\_items:} Vector of input buffers, where each element corresponds to an input port
\item  \textbf{output\_items:} Vector of output buffers, where each element corresponds to an output port
\item \textbf{Returns} the number of items produced at each port
\end{itemize}
\end{frame}

\begin{frame}[fragile]{The general\_work() method}
\begin{minted}[frame=single, fontsize=\tiny]{cpp}
int
general_work(int noutput_items,
       gr_vector_int &ninput_items,
       gr_vector_const_void_star &input_items,
       gr_vector_void_star &output_items);
\end{minted}

\begin{itemize}
\item \textbf{noutput\_items:} The number of output items that this invocation can produce
\item \textbf{ninput\_items:} Vector with the available input items at the corresponding input port
\item \textbf{Returns} the number of items produced at each port
\item Developers \textbf{MUST} call \textbf{consume()} or \textbf{consume\_each()}
 to inform the scheduler the number of consumed items per port
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Retrieve IO ports buffers}
In order to retrieve the buffer of the corresponding port just perform a proper typecast from the \textit{void *} pointer:

\begin{minted}[frame=single, fontsize=\tiny]{cpp}
int
work(int noutput_items,
     gr_vector_const_void_star &input_items,
     gr_vector_void_star &output_items)
{
    /* Get the inputs declared at the constructor io signature */
    const float *in0 = (const float *) input_items[0];
    const gr_complex *in1 = (const gr_complex *) input_items[1];
        
    /* And the output port */        
    gr_complex *out = (gr_complex *) output_items[0];
    ...
}
\end{minted}

\end{frame}

\begin{frame}[fragile]{Access input and output items}
To access items inside a buffer use standard C memory access methods

\begin{minted}[frame=single, fontsize=\tiny]{cpp}
int
work(int noutput_items,
     gr_vector_const_void_star &input_items,
     gr_vector_void_star &output_items)
{
    /* Get the inputs declared at the constructor io signature */
    const float *in0 = (const float *) input_items[0];
    const gr_complex *in1 = (const gr_complex *) input_items[1];
    
    /* And the output port */        
    gr_complex *out = (gr_complex *) output_items[0];
   
   /* We want to propagate the complex items only if the 
    * the corresponding float input items is greater than 0.
    * Otherwise the output should be 0
    */
   memset(out, 0, noutput_items * sizeof(gr_complex));
   for(int i = 0; i < noutput_items; i++){
       if(in0[i] > 0){
           out[i] = in1[i];
       }
   }
   /* We processed all items */
   return noutput_items;
}
\end{minted}

\end{frame}

\begin{frame}{Import block to GRC}
\begin{itemize} \itemsep 1.5em
\item To graphically import a block at the GRC the corresponding \textbf{.yml} file should be written
\item The \textbf{.yml} file provides info about:
\begin{itemize}
\item[-] Block name
\item[-] Parameter values
\item[-] Number and type of IO ports
\item[-] Public setter and getter methods
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Build system}
\begin{itemize} \itemsep 1em
\item GNU Radio and OOT modules use the \textbf{CMake} build system
\item \textbf{CMake} is a tool that automatically produces \textbf{Makefiles}
\item Keeps build files separately from the source code
\item To build and install the OOT module:
\begin{itemize}
\item[-] \textit{cd project\_dir}
\item[-] \textit{mkdir build} (this is necessary only once)
\item[-] \textit{cd build} (this is necessary only once)
\item[-] \textit{cmake ..} (only if you change any of the CMakeLists.txt file)
\item[-] \textit{make}
\item[-] \textit{make install}
\item[-] \textit{make uninstall} (if you want to uninstall)
\end{itemize}
\item After that the new OOT module should be available at the GRC
\end{itemize}
\end{frame}


\begin{frame}{Creating a block from the beginning}
\begin{itemize} \itemsep 1.5em
\item The following slides will guide you to create a new block inside the gr-tutorial
\item The block takes as argument a threshold and a complex input. If the real or the imaginary part is greater than the threshold or less than the -threshold it outputs the threshold or the -threshold respectively. Otherwise the number itself.
\end{itemize}
\end{frame}

\begin{frame}{Creating a block from the beginning}
\begin{itemize} \itemsep 1em
\item Go inside the directory of the OOT module of the class (gr-tutorial)
\item Create a block with name \textit{complex\_clamp} using the gr\_modtool tool
\item \textit{gr\_modtool add complex\_clamp}
\item When asked choose \textbf{sync} block as the block type and \textbf{cpp} for the implementation language
\item Provide the a float parameter for the threshold 
\item For now, skip any QA related files
\end{itemize}
\end{frame}

\begin{frame}{Creating a block from the beginning}
\begin{itemize} \itemsep 1em
\item As this block does not provide any setter and getter, there is no need to change the \textit{include/turorial/complex\_clamp.h} file
\item In the \textit{lib/complex\_clamp\_impl.h} make the appropriate private fields declarations
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}
\begin{minted}[frame=single, fontsize=\tiny]{cpp}
class complex_clamp_impl : public complex_clamp
{
/*
 * Because work() is a method, after the end of its invocation all local variables
 * are lost. So we use private class variables to keep the necessary state
 */
private:
    const float d_threshold;

public:
    complex_clamp_impl(const float threshold);
    ~complex_clamp_impl();

    // Where all the action really happens
    int
    work(int noutput_items, gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
};
\end{minted}
\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}
In the \textit{lib/complex\_clamp\_impl.cc} define the IO signatures and the constructor of the block
\begin{minted}[frame=single, fontsize=\tiny]{cpp}
    complex_clamp_impl::complex_clamp_impl(const float threshold)
      : gr::sync_block("complex_clamp",
              /* The block has exactly one complex input */
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              /* The block has exactly one complex output */
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
              /* Initialize private members */
              d_threshold(threshold)
    {}

\end{minted}
\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}
In the \textit{lib/complex\_clamp\_impl.cc} provide the implementation of the \textbf{work()} method, where the real processing is performed
\begin{minted}[frame=single, fontsize=\tiny]{cpp}
int
complex_clamp_impl::work(int noutput_items,
                         gr_vector_const_void_star &input_items,
                         gr_vector_void_star &output_items)
{
    int i;

    /*Get the input items. NOTE: No modification is allowed on them*/
    const gr_complex *in = (const gr_complex *) input_items[0];
    gr_complex *out = (gr_complex *) output_items[0];
    ...
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}
In the \textit{lib/complex\_clamp\_impl.cc} provide the implementation of the \textbf{work()} method, where the real processing is performed
\begin{minted}[frame=single, fontsize=\tiny]{cpp}
     
     for(i = 0; i < noutput_items; i++){
        out[i] = in[i];
        if(in[i].real() > d_threshold){
            out[i].real(d_threshold);
        }
        
        if (in[i].imag() > d_threshold) {
            out[i].imag(d_threshold);
        }

        if(in[i].real() < -d_threshold){
            out[i].real(-d_threshold);
        }
        
        if (in[i].imag() < -d_threshold) {
            out[i].imag(-d_threshold);
        }
    }
    // Tell runtime system how many output items we produced.
    return noutput_items;
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}
Now edit the \textbf{.yml} file for the GRC that \textit{gr\_modtool} created for you

\begin{minted}[frame=single, fontsize=\tiny]{yaml}
id: tutorial_complex_clamp
label: complex_clamp
category: '[tutorial]'

templates:
  imports: import tutorial
  make: tutorial.complex_clamp(${threshold})

#  Make one 'parameters' list entry for every Parameter you want settable from the GUI.
#     Sub-entries of dictionary:
#     * id (makes the value accessible as \$keyname, e.g. in the make entry)
#     * label
#     * dtype 
parameters:
- id: threshold
  label: Threshold
  dtype: float
.
.
.
\end{minted}

\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}
\begin{minted}[frame=single, fontsize=\tiny]{yaml}
id: tutorial_complex_clamp
label: complex_clamp
category: '[tutorial]'
\end{minted}

\begin{itemize}
\item The \textbf{id} should be unique
\item \textbf{label} is the the string with the block name, displayed at GRC 
\item You can change the label of the block to something nicer. Eg: 
\end{itemize}

\begin{minted}[frame=single, fontsize=\tiny]{yaml}
id: tutorial_complex_clamp
label: Complex Clamp
category: '[tutorial]'
\end{minted}
\end{frame}


\begin{frame}[fragile]{Creating a block from the beginning}

\begin{itemize}
\item At the \textbf{parameters} section add an entry for every parameter of your block
\item \textbf{id} is the unique identifier of the parameter
\item \textbf{label} is the string with the parameter name that GRC will display to the user
\item \textbf{dtype} specifies the type of the variable. Can be \textit{complex}, \textit{float},
\textit{short}, \textit{char}, \textit{int} or \textit{raw}
\end{itemize}

\begin{minted}[frame=single, fontsize=\tiny]{yaml}
parameters:
- id: threshold
  label: Threshold
  dtype: float
\end{minted}
\end{frame}


\begin{frame}[fragile]{Creating a block from the beginning}

\begin{itemize}
\item Now we should specify the input and output ports of the block
\item \textbf{domain} can be either \textit{stream} or \textit{message} for message passing ports
\item \textbf{dtype} specifies the type of the port. Can be \textit{complex}, \textit{float},
\textit{short}, \textit{char} or \textit{int}
\end{itemize}

\begin{minted}[frame=single, fontsize=\tiny]{yaml}
inputs:
- label: in
  domain: stream
  dtype: complex

outputs:
- label: out
  domain: stream
  dtype: complex
\end{minted}
\end{frame}

\begin{frame}[fragile]{Creating a block from the beginning}

\begin{itemize}
\item Now it is time to compile and install the module!
\item Follow the build and install instructions located at the README of the \textbf{gr-tutorial} module
\item Close and open GRC or use the \textbf{Reload} button in order the new changes to take effect
\end{itemize}

\end{frame}

\begin{frame}{Creating a block from the beginning}

\begin{center}
\includegraphics[width=0.9\textwidth]{images/block.png}
\end{center}
\end{frame}

\begin{frame}{Creating a block from the beginning}

Use the \textit{examples/clamp.grc} flowgraph to test the result!
\begin{center}
\includegraphics[width=0.9\textwidth]{images/example.png}
\end{center}
\end{frame}


\end{document}
