\documentclass{beamer}
\usetheme{metropolis}
\metroset{progressbar=frametitle, background=light}
\usepackage{appendixnumberbeamer}
\usepackage{textcomp}
\usepackage{ marvosym }
\usepackage{wasysym}
\usepackage{svg}
\usepackage{pgfplots}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\usepackage[absolute,overlay]{textpos}
\usepackage{graphicx}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\title{Signal Processing Basics \& Filtering}
%\date{\today}
\author{Manolis Surligas \quad \newline\href{mailto:surligas@csd.uoc.gr}{surligas@csd.uoc.gr}}
\institute{Computer Science Department, University of Crete}


\begin{document}

{
\setbeamercolor{background canvas}{bg=black}
\maketitle
}

\begin{frame}[standout]
\begin{center}
This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License

\ccbyncsaeu

\vspace{1em}
\footnotesize
\url{https://gitlab.com/surligas/sdr-tutorial}
\url{https://gitlab.com/surligas/gr-tutorial}
\end{center}
\end{frame}

\section{Signal Representation}
\begin{frame}{Signal Representation}
Signals can be represented in the \textbf{time domain}
\includegraphics[width=\textwidth]{images/time_domain.png} 
\end{frame}

\begin{frame}{Signal Representation}
...or in the \textbf{frequency domain}
\includegraphics[width=\textwidth]{images/freq_domain.png} 
\end{frame}

\begin{frame}{Signal Representation}
\textbf{Time domain:}
\begin{itemize}
\item X-axis represents the time
\item Y-axis represents the amplitude (e.g Volts)
\end{itemize}

\textbf{Frequency domain:}
\begin{itemize}
\item X-axis represents the frequency
\item Y-axis represents the power
\item Commonly power is expressed in logarithmic scale ($10 \times log_{10}(x)$)

\end{itemize}
\end{frame}

\begin{frame}{Signal Representation}
\textbf{Frequency domain:}
\begin{itemize}
\item For the transition from time to frequency domain, the Discrete Fourier Transform (DFT) $X_k$ is used
\end{itemize}
\begin{align*}
  X_k &= \sum_{n=0}^{N-1} x_n \cdot e^{-\frac {i 2\pi}{N}kn}\\
      &= \sum_{n=0}^{N-1} x_n \cdot \left[\cos\left(\frac{2 \pi}{N}kn\right) - i \cdot \sin\left(\frac{2 \pi}{N}kn\right)\right]
\end{align*}
\begin{itemize}
\item In simple words, the $X_k$ tranformation, converts the original signal into $N$ cosines, each one with a frequency $\frac{2 \pi}{N}kn$ and amplitude $x_n$
\item For real signals the imaginary part is 0
\end{itemize}
\end{frame}


\begin{frame}{Signal Representation}
\begin{itemize} \itemsep 1em
\item Changing the signal in the time domain affects the frequency domain
\item In most cases we use the frequency domain representation
\item Different frequency components can be spotted very easily
\item Signals can be spotted visually even with significant amount of noise
\end{itemize}

\begin{block}{}
Use the \textbf{simple\_example.grc} flowgraph located at the \textbf{examples} directory to play with time and frequency domain representation
\end{block}
\end{frame}


\begin{frame}{Signal Representation}

\textbf{Example 1:} Finding the frequency components of a mixed signal
\begin{columns}[T]
\column{0.4\textwidth}
\includegraphics[width=\textwidth]{images/sines_time_domain.png} 
\column{0.6\textwidth}
\includegraphics[width=\textwidth]{images/sines_freq_domain.png} 
\end{columns}

\begin{block}{}
The two different sine waves can be spotted quite easily in the frequency domain comparing to the time domain
\end{block}
\end{frame}

\begin{frame}{Signal Representation}

\textbf{Example 2:} Finding the frequency components of a noisy mixed signal
\begin{columns}[T]
\column{0.5\textwidth}
\includegraphics[width=\textwidth]{images/noisy_sines_time_domain.png} 
\column{0.5\textwidth}
\includegraphics[width=\textwidth]{images/noisy_sines_freq_domain.png} 
\end{columns}

\begin{block}{}
Even with the presence of significant noise, the signal components are distinguishable!
(However this is not always the case \frownie{})
\end{block}
\end{frame}

\section{Complex Numbers and IQ representation}
\begin{frame}{Complex Numbers and IQ representation}
Lets plot the frequency domain of a $\cos()$ signal using the \textbf{iq\_example.grc}
\begin{figure}
\centering
\includegraphics[width=0.55\textwidth]{images/real_cos.png} 
\end{figure}
Even if the example used a single $\cos(x)$ signal at 1 kHz there is another one at -1 kHz!!!
\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{columns}
\begin{column}{0.4\textwidth}
The usual suspect! Trigonometric identities! 
\begin{itemize}
\item $\sin(-\theta) = -\sin \theta$
\item $\cos(-\theta) =+ \cos \theta$
\end{itemize}
\end{column}
\begin{column}{0.6\textwidth}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/iq_issue.pdf} 
\end{figure}
\end{column}
\end{columns}

\begin{block}{}
Recall the DFT and how it transforms a time domain signal into a summation of cosines.
Due to the above identities, a real signal at frequency $f$ should produce a mirrored signal at $-f$ at the frequency domain
\end{block}

\end{frame}


\begin{frame}{Complex Numbers and IQ representation}
\begin{itemize}
\item We showed that \textbf{real} signals will always have positive and negative spectral components
\item In baseband applications (eg. audio), this is not a problem, as there is no notion of a negative frequency
\item What about RF signals? 
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/baseband_rf.pdf} 
\end{figure}
\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{itemize}
\item Having mirroring on RF signals is not practical
\item We do not want to occupy bandwidth to repeat the same information
\item \textbf{The solution:} IQ representation
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/rf.pdf} 
\end{figure}
\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{itemize}
\item We showed that \textbf{real} signals will always have positive and negative spectral components
\item In baseband applications (eg. audio), this is not a problem, as there is no notion of a negative frequency
\item What about RF signals? 
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/baseband_rf.pdf} 
\end{figure}
\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{itemize}
\item Having mirroring on RF signals is not practical
\item We do not want to occupy bandwidth to repeat the same information
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/rf.pdf} 
\end{figure}
\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{alertblock}{The solution!}
Use the IQ representation
\begin{align*}
  x(t) &= e^{-j 2 \pi f t}\\
       &= \underbrace{\cos(2 \pi f t)}_{\text{In-phase(I)}} - j\overbrace{\sin(2 \pi f t)}^{\text{Quadrature (Q)}}
\end{align*}
\end{alertblock}
\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{block}{}
Adding the quadrature component removes the ubiquity of the $\cos(\theta) = \cos(-\theta)$
\end{block}

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{images/iq.pdf} 
\end{figure}

\end{frame}

\begin{frame}{Complex Numbers and IQ representation}
\begin{alertblock}{Example!}
Use the \textbf{iq\_representation.grc} flowgraph to visually identify complex numbers and IQ representation
\end{alertblock}
\end{frame}

\section{Sampling \& Quantization}
\begin{frame}{Sampling Theory}
\textbf{Sampling} is the process of converting an continuous-time signal into a discrete-time signal
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/Sampled_signal.png} 
\end{figure}

\end{frame}

\begin{frame}{Sampling Theory}

\begin{alertblock}{Nyquist Theorem}
\vspace{1em}
A real signal $x(t)$ band-limited to $B$ Hz can be reconstructed without loss of information iff the sampling frequency is greater than $2 \times B$
\end{alertblock}

\begin{itemize} \itemsep 1em
\item A band-limited signal $x(t)$, is a signal that does not have spectral components above $B$ Hz
\item The minimum sampling frequency $R=2 \times B$ is called \textbf{Nyquist} frequency
\end{itemize}
\end{frame}

\begin{frame}{Sampling Theory}
\begin{itemize} \itemsep 1em
\item In most cases there is a need to change the sampling rate of a signal
\begin{itemize}
\item Hardware capabilities / restrictions
\item Processing maybe easier at specific sampling rates
\item Processing resources
\end{itemize}
\item \textbf{Decimation} is the process of reducing the sample rate 
\item \textbf{Interpolation} increases the sampling rate
\item Combining decimation and interpolation an arbitrary sampling rate can be achieved
\item More about resampling on the next lecture!
\end{itemize}
\end{frame}

\begin{frame}{Quantization}
\begin{figure}
\centering
\textbf{Quantization} converts the signal amplitude into digital form samples

\includegraphics[width=0.6\textwidth]{images/quantization.png} 
\end{figure}

\begin{block}{}
An approximation of the Signal-to-Quantization Noise Ratio (SQNR) for an N-bit ADC/DAC is: \newline
$\mathrm{SQNR} = 20 \log_{10}(2^N) \approx 6.02 \cdot N\ \mathrm{dB}$
\end{block}
\end{frame}

\section{Filtering}
\begin{frame}{Why do we use filters?}
\begin{itemize} \itemsep 1.5em
\item Filters are used to separate mixed signals
\item Extract a specific band of interest
\item Avoid undesired effects (aliasing, imaging e.t.c)
\item Restore distorted signals
\item Pulse shaping
\end{itemize}

\end{frame}

\begin{frame}{Filter types}
\textbf{Analog Filters:}
\begin{itemize} \itemsep 0.5em
\item[-] Implemented using electronic components (resistors, capacitors, coils, transistors)
\item[-] Used in analog front-ends
\item[-] Standard performance
\item[-] Difficult to change their properties
\item[-] Application specific
\end{itemize}
\includegraphics[width=0.5\linewidth]{images/filter_rc.png}
\end{frame}

\begin{frame}{Filter types}
\textbf{Digital Filters:}
\begin{itemize} \itemsep 0.5em
\item[-] Implemented in software
\item[-] Variable performance depending the processing resources
\item[-] Change their properties relatively easily
\item[-] Can adapt to any application
\end{itemize}
\includegraphics[width=0.3\linewidth]{images/filter_digital.png}
\end{frame}

\begin{frame}{Filter types}
\begin{itemize} \itemsep 1.5em
\item Low Pass Filters
\begin{itemize}
\item[-] Frequencies above a threshold do not pass the filter
\end{itemize}
\includegraphics[width=0.8\linewidth]{images/low_pass.png}
\end{itemize}
\end{frame}

\begin{frame}{Filter types}
\begin{itemize} \itemsep 1.5em
\item High Pass Filters
\begin{itemize}
\item[-] Frequencies below a threshold do not pass the filter
\end{itemize}
\includegraphics[width=0.8\linewidth]{images/high_pass.png}
\end{itemize}
\end{frame}

\begin{frame}{Filter types}
\begin{itemize} \itemsep 1.5em
\item Band Pass Filters
\begin{itemize}
\item[-] Frequencies of a specific band pass the filter
\end{itemize}
\includegraphics[width=0.8\linewidth]{images/band_pass.png}
\end{itemize}
\end{frame}

\begin{frame}{Filter parameters}
\begin{itemize} \itemsep 1em
\item \textbf{Cutoff frequency:} The frequency above (low pass filter) or below (high pass filter) which, the filter starts to attenuate the signal
\item \textbf{Transition width:} Control how steep is the attenuation of the signal above (low pass filter) or below (high pass filter) the cutoff frequency
\end{itemize}
\end{frame}

\begin{frame}{Filter parameters}
\begin{itemize}
\item \textbf{Small transition width:}

\includegraphics[width=0.5\linewidth]{images/small_tr_width.png}
\item \textbf{Large transition width:}

\includegraphics[width=0.5\linewidth]{images/large_tr_width.png}
\end{itemize}
\end{frame}

\begin{frame}{Filtering and CPU resources}
\begin{itemize}
\item Transition width specifies the number of taps of the filter
\item Steep filters have more taps than relaxed ones 
\item Designing a filter is always a balance between RF performance and CPU utilization
\item Sampling rate plays a key role too
\end{itemize}
\end{frame}

\begin{frame}{Filtering and CPU resources}
\begin{exampleblock}{Example}
\begin{itemize}
\item Assume a sampling rate of 1 MSPS
\item We are interested in a range of only 10 kHz
\item The requirements demand a very steep filter and we have limited CPU resources (e.g RPi3)
\item How???
\pause
\item Use a relaxed first stage relaxed filter
\item Decimate the signal by a factor of e.g 50
\item Apply a second stage steep filtering 
\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}{Pulse Shaping Filtering}
\begin{itemize}
\item Filters can be used also for pulse shaping
\item The goal is to minimize spurious signals and increase the spectral efficiency
\item Spurious signals can be generated by extreme transitions of the source signal
\item A simple square wave generated from a bit stream has an infinite bandwidth \MVRightarrow non practical to transmit 
\end{itemize}
\end{frame}

\begin{frame}{Pulse Shaping Filtering}
\begin{itemize}
\item A simple square wave generated from a bit stream has an infinite bandwidth \MVRightarrow non practical to transmit
\item A square wave is an infinite sum of sine waves 
\end{itemize}
\begin{columns}[T]
\column{0.5\textwidth}
\includegraphics[width=\textwidth]{images/sq_time.png} 
\column{0.5\textwidth}
\includegraphics[width=\textwidth]{images/sq_freq.png} 
\end{columns}
\end{frame}

\begin{frame}{Pulse Shaping Filtering}
\begin{itemize}
\item Commonly for shaping we use the Root Raised Cosine filter (RRC)
\item Smooths the extreme transitions thus reducing the spurious 
\end{itemize}
\begin{columns}[T]
\column{0.5\textwidth}
\includegraphics[width=\textwidth]{images/sq_time_shaped.png} 
\column{0.5\textwidth}
\includegraphics[width=\textwidth]{images/sq_freq_shaped.png} 
\end{columns}
\end{frame}

\end{document}
